Name: Oluwatooni Adebiyi

I used K Nearest Neighbour Algorithm for the Recommender System.

It's deployed on Heroku here http://toonimovierecommender.herokuapp.com/

It recommends movies for users using two methods

First is Movie based. It recommends movies that are similar to the movie selected

Second is criteria based. It recommends movies based on Genres selected
